package info.devcodenet.interviewquestion;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);

        new Thread()
        {
            @Override
            public void run() {
                try
                {
                    sleep(2000);
                    startActivity(new Intent(SplashScreen.this,MainActivity.class));
                    ActivityCompat.finishAffinity(SplashScreen.this);
                }catch (Exception ex)
                {

                }
            }
        }.start();
    }
}
