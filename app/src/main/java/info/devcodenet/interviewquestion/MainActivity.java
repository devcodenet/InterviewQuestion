package info.devcodenet.interviewquestion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent=new Intent(this,WebViewActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure want to quit")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.finishAffinity(MainActivity.this);
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                return true;
            case R.id.action_about:
                Intent i=new Intent(this,About.class);
                startActivity(i);
                return true;
            case R.id.action_policy:
                startActivity(new Intent(MainActivity.this,Policy.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void android(View view) {
        intent.putExtra("url","android.htm");
        startActivity(intent);
    }

    public void angular(View view) {
        intent.putExtra("url","angular_js.htm");
        startActivity(intent);
    }

    public void bootstrap(View view) {
        intent.putExtra("url","Bootstrap.htm");
        startActivity(intent);
    }

    public void cprogram(View view) {
        intent.putExtra("url","C_Programming.htm");
        startActivity(intent);
    }

    public void cplus(View view) {
        intent.putExtra("url","cplus.htm");
        startActivity(intent);
    }

    public void cics(View view) {
        intent.putExtra("url","cisc.htm");
        startActivity(intent);
    }

    public void cobol(View view) {
        intent.putExtra("url","cobol.htm");
        startActivity(intent);
    }


    public void dataalgorithm(View view) {
        intent.putExtra("url","data_str.htm");
        startActivity(intent);
    }

    public void design(View view) {
        intent.putExtra("url","design.htm");
        startActivity(intent);
    }

    public void ejb(View view) {
        intent.putExtra("url","ejb.htm");
        startActivity(intent);
    }

    public void go(View view) {
        intent.putExtra("url","go.htm");
        startActivity(intent);
    }

    public void gwt(View view) {
        intent.putExtra("url","gwt.htm");
        startActivity(intent);
    }

    public void hadoop(View view) {
        intent.putExtra("url","hadoop.htm");
        startActivity(intent);
    }

    public void hibernate(View view) {
        intent.putExtra("url","hibernate.htm");
        startActivity(intent);
    }

    public void hive(View view) {
        intent.putExtra("url","hive.htm");
        startActivity(intent);
    }

    public void hbase(View view) {
        intent.putExtra("url","hbase.htm");
        startActivity(intent);
    }

    public void html(View view) {
        intent.putExtra("url","html5.htm");
        startActivity(intent);
    }

    public void ims(View view) {
        intent.putExtra("url","ims.htm");
        startActivity(intent);
    }

    public void java(View view) {
        intent.putExtra("url","java.htm");
        startActivity(intent);
    }

    public void javaxml(View view) {
        intent.putExtra("url","javaxml.htm");
        startActivity(intent);
    }

    public void jsp(View view) {
        intent.putExtra("url","JSP.htm");
        startActivity(intent);
    }

    public void jdbc(View view) {
        intent.putExtra("url","JDBC.htm");
        startActivity(intent);
    }

    public void javascript(View view) {
        intent.putExtra("url","js.htm");
        startActivity(intent);
    }

    public void jquerry(View view) {
        intent.putExtra("url","Jquery.htm");
        startActivity(intent);
    }


    public void jcl(View view) {
        intent.putExtra("url","JCL.htm");
        startActivity(intent);
    }

    public void junit(View view) {
        intent.putExtra("url","Junit.htm");
        startActivity(intent);
    }

    public void javanew(View view) {
        intent.putExtra("url","java8.htm");
        startActivity(intent);
    }

    public void logj(View view) {
        intent.putExtra("url","log4j.htm");
        startActivity(intent);
    }

    public void maven(View view) {
        intent.putExtra("url","Maven.htm");
        startActivity(intent);
    }

    public void mvc(View view) {
        intent.putExtra("url","MVC.htm");
        startActivity(intent);
    }

    public void mongodb(View view) {
        intent.putExtra("url","MongoDB.htm");
        startActivity(intent);
    }

    public void nodejs(View view) {
        intent.putExtra("url","Node_js.htm");
        startActivity(intent);
    }

    public void obiee(View view) {
        intent.putExtra("url","Obiee.htm");
        startActivity(intent);
    }

    public void perl(View view) {
        intent.putExtra("url","Perl.htm");
        startActivity(intent);
    }

    public void python(View view) {
        intent.putExtra("url","Python.htm");
        startActivity(intent);
    }

    public void php(View view) {
        intent.putExtra("url","PHP.htm");
        startActivity(intent);
    }

    public void plsql(View view) {
        intent.putExtra("url","Pl_SQL.htm");
        startActivity(intent);
    }

    public void qc(View view) {
        intent.putExtra("url","QC.htm");
        startActivity(intent);
    }

    public void qtp(View view) {
        intent.putExtra("url","QTP.htm");
        startActivity(intent);
    }

    public void restful(View view) {
        intent.putExtra("url","RESTful.htm");
        startActivity(intent);
    }

    public void servelet(View view) {
        intent.putExtra("url","Servlets.htm");
        startActivity(intent);
    }

    public void sqoop(View view) {
        intent.putExtra("url","Sqoop.htm");
        startActivity(intent);
    }

    public void struts(View view) {
        intent.putExtra("url","Struts2.htm");
        startActivity(intent);
    }

    public void sapabap(View view) {
        intent.putExtra("url","SAP_ABAP.htm");
        startActivity(intent);
    }

    public void sapbasic(View view) {
        intent.putExtra("url","SAP_BASIS.htm");
        startActivity(intent);
    }

    public void sapbods(View view) {
        intent.putExtra("url","SAP_BODS.htm");
        startActivity(intent);
    }

    public void sapbw(View view) {
        intent.putExtra("url","SAP_BW.htm");
        startActivity(intent);
    }

    public void sapcca(View view) {
        intent.putExtra("url","SAP_CCA.htm");
        startActivity(intent);
    }

    public void sapcrm(View view) {
        intent.putExtra("url","SAP_CRM.htm");
        startActivity(intent);
    }

    public void sapewm(View view) {
        intent.putExtra("url","SAP_EWM.htm");
        startActivity(intent);
    }

    public void sapfico(View view) {
        intent.putExtra("url","SAP_FICO.htm");
        startActivity(intent);
    }

    public void sapfiori(View view) {
        intent.putExtra("url","SAP_Fiori.htm");
        startActivity(intent);
    }

    public void saphana(View view) {
        intent.putExtra("url","SAP_HANA.htm");
        startActivity(intent);
    }

    public void sapmm(View view) {
        intent.putExtra("url","SAP_MM.htm");
        startActivity(intent);
    }

    public void sapidt(View view) {
        intent.putExtra("url","SAP_IDT.htm");
        startActivity(intent);
    }

    public void saplum(View view) {
        intent.putExtra("url","SAP_Lumira.htm");
        startActivity(intent);
    }

    public void sappi(View view) {
        intent.putExtra("url","SAP_PI.htm");
        startActivity(intent);
    }

    public void sapqm(View view) {
        intent.putExtra("url","SAP_QM.htm");
        startActivity(intent);
    }

    public void sapscm(View view) {
        intent.putExtra("url","SAP_SCM.htm");
        startActivity(intent);
    }

    public void sapsd(View view) {
        intent.putExtra("url","SAP_SD.htm");
        startActivity(intent);
    }

    public void sapsol(View view) {
        intent.putExtra("url","SAP_Solman.htm");
        startActivity(intent);
    }

    public void sapsrm(View view) {
        intent.putExtra("url","SAP_SRM.htm");
        startActivity(intent);
    }

    public void sapweb(View view) {
        intent.putExtra("url","SAP_WEB.htm");
        startActivity(intent);
    }

    public void sapwebi(View view) {
        intent.putExtra("url","CSAP_WEBi.htm");
        startActivity(intent);
    }

    public void sql(View view) {
        intent.putExtra("url","SQL.htm");
        startActivity(intent);
    }

    public void svg(View view) {
        intent.putExtra("url","SVG.htm");
        startActivity(intent);
    }

    public void hrinter(View view) {
        intent.putExtra("url","hr_interview_questions.htm");
        startActivity(intent);

    }
    public void hrbrain(View view) {
        intent.putExtra("url","brainteaser_interview_questions.htm");
        startActivity(intent);
    }

    public void imsdb(View view) {
        intent.putExtra("url","ims.htm");
        startActivity(intent);
    }

    public void db(View view) {
        intent.putExtra("url","db2.htm");
        startActivity(intent);
    }

    public void csharp(View view) {
        intent.putExtra("url","csharp.htm");
        startActivity(intent);
    }
}
